﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSprites : MonoBehaviour {

    public static GameSprites instance;

    public Sprite star;
    public Sprite ship;
    public Sprite bullet;
    public Sprite enemy;

	// Use this for initialization
	void Awake () {
        instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
