﻿
using System;
using System.Collections.Generic;
using ECS;

public class ItemSystem : ECSSystem
{

    [Inject(typeof(TransformComp))]
    private Match match;

    [Inject]
    private ComponentTracker<TransformComp> transforms;

    public ItemSystem(ECSRoot root) : base(root)
    {
    }

    public override void Update(float delta)
    {
        for (int i = 0; i < match.Length; i++)
        {
            TransformComp tr = transforms.ComponentsByEntity[match.Entities[i]];
            //UnityEngine.Debug.Log("ItemSystem: MatcherID: " + match.MatcherID);
        }

    }
}
