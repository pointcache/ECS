﻿
using System;
using System.Collections.Generic;
using UnityEngine;
using ECS;

public class TransformSystem : ECSSystem,
    IComponentAddedReceiver<TransformComp>,
    IComponentRemovedReceiver<TransformComp>
{

    [Inject(typeof(TransformComp))]
    private Match transformMatch;

    [Inject]
    private ComponentTracker<TransformComp> transforms;



    private Transform[] transformPool;
    private int freetransformsCount = 0;
    private int transformsPerBatch = 100;

    public override void Awake()
    {
        transformPool = new Transform[0];
    }

    public TransformSystem(ECSRoot root) : base(root)
    {
    }

    public void OnCompAdded(TransformComp comp, Entity entity)
    {
        //Debug.Log(123);
    }

    public void OnCompRemoved(TransformComp comp, Entity entity)
    {

    }

    public override void Update(float delta)
    {
        for (int i = 0; i < this.transformMatch.Length; i++)
        {
            Entity e = this.transformMatch[i];
            TransformComp tr = this.transforms[e.ID];
            continue;
        }
    }

    private void ExpandPool()
    {

    }
}
