﻿
using System;
using System.Collections.Generic;
using ECS;
using UnityEngine;

[System.Serializable]
public class TransformComp : Comp
{
    public static readonly int typeid;
    public override int Typeid
    {
        get
        {
            return typeid;
        }
    }

    public Transform unityTransform;

    public override void Reset()
    {

    }
}
