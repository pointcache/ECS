﻿
using System;
using System.Collections.Generic;
using ECS;

public class ItemComp : Comp
{
    public static readonly int typeid;
    public override int Typeid
    {
        get
        {
            return typeid;
        }
    }

    public string Name;

    public override void Reset()
    {
        this.Name = null;
    }
}
