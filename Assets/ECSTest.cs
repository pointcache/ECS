﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ECS;

public class ECSTest : MonoBehaviour
{

    [SerializeField]
    private ECSDebugGUI debugGUI;

    [SerializeField]
    private bool drawUI = false;

    public ECSRoot ECS { get; private set; }

    public Mode mode = Mode.normal;
    public enum Mode
    {
        normal,
        stress,
        five_mil
    }

    // Use this for initialization
    void Start()
    {
        this.ECS = new ECSRoot();

        switch (this.mode)
        {
            case Mode.normal:
            {
                this.ECS.SystemsController.AddSystem<TransformSystem>();
                this.ECS.SystemsController.AddSystem<ItemSystem>();
            }
            break;
            case Mode.stress:
            {
                this.ECS.SystemsController.AddSystem<StressTest>();
            }
            break;
            case Mode.five_mil:
            {
                this.ECS.SystemsController.AddSystem<TransformSystem>();
                for (int i = 0; i < 5000000; i++)
                {
                    SpawnEntity();
                }
            }
            break;
            default:
            break;
        }

        if (this.debugGUI)
        {
            this.debugGUI.SetRoot(this.ECS);
        }
    }

    private void Update()
    {
        this.ECS.Update();
        if (this.debugGUI)
        {
            if (this.ECS.EntityManager.Count < 1000)
            {
                this.debugGUI.gameObject.SetActive(this.drawUI);
            }
            else
            {
                this.debugGUI.gameObject.SetActive(false);
            }
        }
    }


    private void SpawnEntity()
    {
        Entity e = this.ECS.EntityManager.CreateEntity("Test Entity");
        var test = ComponentPool<TransformComp>.Get();
        //test.unityTransform = new GameObject(e.Name).transform;
        e.AddComponent(test);
    }

    private void OnGUI()
    {
        GUI.Label(new Rect(0, 0, 200, 30), this.ECS.EntityManager.Count.ToString());

        if (GUI.Button(new Rect(300, 20, 100, 30), "Add"))
        {
            SpawnEntity();
        }

        if (GUI.Button(new Rect(400, 20, 100, 30), "Add 1k"))
        {
            for (int i = 0; i < 1000; i++)
            {
                SpawnEntity();
            }
        }

        if (GUI.Button(new Rect(500, 20, 100, 30), "Add 10k"))
        {
            for (int i = 0; i < 10000; i++)
            {
                SpawnEntity();
            }
        }

        if (GUI.Button(new Rect(600, 20, 100, 30), "Add 100k"))
        {
            for (int i = 0; i < 100000; i++)
            {
                SpawnEntity();
            }
        }

        if (GUI.Button(new Rect(700, 20, 100, 30), "Add 100k datacomp"))
        {
            for (int i = 0; i < 100000; i++)
            {
                SpawnEntity();
            }
        }

        if (GUI.Button(new Rect(800, 20, 100, 30), "Destroy All"))
        {
            ECS.EntityManager.DestroyAllEntities();
        }
    }

}

