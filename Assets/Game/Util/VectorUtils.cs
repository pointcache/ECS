﻿
using System;
using System.Collections.Generic;
using UnityEngine;
using static Unity.Mathematics.math;
using Unity.Mathematics;

public static class VectorUtils
{
    public static float2 GetForward(Position pos)
    {
        float h = radians(pos.rotation);
        float y = cos(h);
        float x = sin(h);

        return new float2(-x, y);
    }

    public static float2 GetDirection(float2 pos, float2 target)
    {
        return normalize(target - pos);
    }

    public static float DirectionToRotation(float2 direction)
    {
        float2 normalized = new float2(0, 1);
        float2 normalized2 = normalize(direction);
        float num = acos(clamp(dot(normalized, normalized2), -1f, 1f)) * 57.29578f;
        float num2 = sign(normalized.x * normalized2.y - normalized.y * normalized2.x);
        float result = num * (num2 >= 0f ? 1f : -1f);
        return result;
    }

    public static float LookAt(float2 origin,  float2 target)
    {
        return DirectionToRotation(GetDirection(origin, target));
    }
}
