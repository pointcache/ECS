﻿

using System;
using System.Collections.Generic;
using UnityEngine;

public class VectorUtilsMathf
{
    public static Vector2 GetForward(Position pos)
    {
        float h = Mathf.Deg2Rad * pos.rotation;
        float y = Mathf.Cos(h);
        float x = Mathf.Sin(h);

        return new Vector2(-x, y);
    }

    public static Vector2 GetDirection(Vector2 pos, Vector2 target)
    {
        return (target - pos).normalized;
    }

    public static float DirectionToRotation(Vector2 direction)
    {
        Vector2 normalized = new Vector2(0, 1);
        Vector2 normalized2 = direction.normalized;
        float num = Mathf.Acos(Mathf.Clamp(Vector2.Dot(normalized, normalized2), -1f, 1f)) * 57.29578f;
        float num2 = Mathf.Sign(normalized.x * normalized2.y - normalized.y * normalized2.x);
        float result = num * num2;
        return result;
    }
}
