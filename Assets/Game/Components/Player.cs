﻿// Alexander Antonov, www.pointcache.com all rights reserved. 

using System;
using System.Collections.Generic;
using ECS;

public class Player : SingletonComp
{
    public static readonly int typeid;
    public override int Typeid => typeid;

    public float ammo;
    public float speed;

    public override void Reset()
    {
        ammo = speed = 0;
    }
}
