﻿using System;
using System.Collections.Generic;
using ECS;

[System.Serializable]
public class MoveForward : Comp
{
    public static readonly int typeid;
    public override int Typeid => typeid;

    public float speed;

    public override void Reset()
    {
        speed = 0;
    }
}
