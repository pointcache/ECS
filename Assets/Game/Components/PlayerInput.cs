﻿// Alexander Antonov, www.pointcache.com all rights reserved. 

using System;
using System.Collections.Generic;
using ECS;
using Unity.Mathematics;
using UnityEngine;

public class PlayerInput : SingletonComp
{
    public static readonly int typeid;
    public override int Typeid => typeid;

    public float Horizontal;
    public float Vertical;
    public float2 mousePos;
    public float2 mousePosWorld;

    public KeyEvent Shoot;

    [System.Serializable]
    public struct KeyEvent
    {
        public bool Down;
        public bool Held;
        public bool Up;

        public KeyEvent(KeyCode key)
        {
            Down = Input.GetKeyDown(key);
            Held = Input.GetKey(key);
            Up = Input.GetKey(key);
        }
    }

    public override void Reset()
    {
        Horizontal = Vertical = 0f;
        Shoot = new KeyEvent();
    }
}
