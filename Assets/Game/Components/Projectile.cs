﻿// Alexander Antonov, www.pointcache.com all rights reserved. 

using System;
using System.Collections.Generic;
using ECS;

public class Projectile : Comp
{
    public static readonly int typeid;
    public override int Typeid => typeid;

    public float Damage;

    public override void Reset()
    {

    }
}
