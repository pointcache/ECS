﻿using System;
using System.Collections.Generic;
using ECS;

[System.Serializable]
public class EnemyAI : Comp
{
    public static readonly int typeid;
    public override int Typeid => typeid;

    public int target;
    public float timeBetweenShots;
    public float speed;

    public override void Reset()
    {

    }
}
