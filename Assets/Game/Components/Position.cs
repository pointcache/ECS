﻿// Alexander Antonov, www.pointcache.com all rights reserved. 

using System;
using System.Collections.Generic;
using ECS;
using UnityEngine;

public class Position : Comp
{
    public static readonly int typeid;
    public override int Typeid => typeid;

    public float x, y;
    public float rotation;

    public Vector2 position
    {
        get { return new Vector2(x, y); }
        set
        {
            x = value.x;
            y = value.y;
        }
    }
    public Vector2 forward => VectorUtils.GetForward(this);

    public void Rotate(float value)
    {
        float result = rotation - value;
        if (result > 360)
        {
            rotation = result - 360;
        }
        else if (result < 0)
        {
            rotation = 360 - result;
        }
        else
        {
            rotation = result;
        }
    }

    public override void Reset()
    {
        x = y = rotation = 0;
    }
}
