﻿// Alexander Antonov, www.pointcache.com all rights reserved. 

using System;
using System.Collections.Generic;
using ECS;

public class WorldObject: Comp
{
    public static readonly int typeid;
    public override int Typeid => typeid;
    public WorldGameObjectTemplate template;

    public override void Reset()
    {
        template = null;
    }
}
