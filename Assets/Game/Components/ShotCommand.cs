﻿using System;
using System.Collections.Generic;
using ECS;
using UnityEngine;

[System.Serializable]
public class ShotCommand : SharedComp
{
    public static readonly int typeid;
    public override int Typeid => typeid;

    public override void Reset()
    {
    }
}
