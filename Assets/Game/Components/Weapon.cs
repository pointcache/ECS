﻿using System;
using System.Collections.Generic;
using ECS;

[System.Serializable]
public class Weapon : Comp
{
    public static readonly int typeid;
    public override int Typeid => typeid;

    public enum Projectile
    {
        player,
        enemy
    }

    public Projectile projectile;
    public float damage;
    public float direction;
    public float speed;

    public override void Reset()
    {

    }
}
