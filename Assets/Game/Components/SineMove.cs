﻿// Alexander Antonov, www.pointcache.com all rights reserved. 

using System;
using System.Collections.Generic;
using ECS;

public class SineMove : Comp
{
    public static readonly int typeid;
    public override int Typeid => typeid;


    public float sineAccumulator;

    public override void Reset()
    {
        sineAccumulator = 0;
    }
}
