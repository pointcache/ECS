﻿using System;
using System.Collections.Generic;
using ECS;
using UnityEngine;

public class ShootingSystem : ECSSystem
{

    [Inject(typeof(ShotCommand), typeof(Weapon), typeof(Position))]
    private Match match;

    [Inject] private ComponentTracker<Weapon> weapons;
    [Inject] private ComponentTracker<Position> positions;

    public ShootingSystem(ECSRoot root) : base(root)
    {
    }

    public override void Update(float delta)
    {
        for (int i = 0; i < match.Length; i++)
        {
            Entity e = this.entityManager.GetEntity(match.Entities[i]);

            Weapon weapon = weapons.ComponentsByEntity[e.ID];
            Position position = positions.ComponentsByEntity[e.ID];

            switch (weapon.projectile)
            {
                case Weapon.Projectile.player:
                {
                    SpawnPlayerBullet(position, weapon);
                }
                break;
                case Weapon.Projectile.enemy:
                break;
                default:
                break;
            }

            entityManager.DestroyComponent(ShotCommand.typeid, e);
        }
    }

    private void SpawnPlayerBullet(Position playerPos, Weapon weapon)
    {
        Entity e = entityManager.CreateEntity();

        Position position = ComponentPool<Position>.Get();
        position.x = playerPos.x;
        position.y = playerPos.y;
        position.rotation = playerPos.rotation;

        SpriteComp spriteComp = ComponentPool<SpriteComp>.Get();
        spriteComp.sprite = GameSprites.instance.bullet;
        spriteComp.color = Color.cyan;

        Size size = ComponentPool<Size>.Get();
        size.size = new Vector2(0.5f, 0.5f);
        size.layer = Size.CollisionLayer.Projectile;

        MoveForward mf = ComponentPool<MoveForward>.Get();
        mf.speed = weapon.speed;

        Projectile bullet = ComponentPool<Projectile>.Get();
        bullet.Damage = weapon.damage;

        Lifetime lifetime = ComponentPool<Lifetime>.Get();
        lifetime.lifetimeLeft = 3f;

        e.AddComponent(position);
        e.AddComponent(spriteComp);
        e.AddComponent(size);
        e.AddComponent(mf);
        e.AddComponent(bullet);
        e.AddComponent(lifetime);
        e.Name = "PlayerBullet";
        e.AddComponent(WorldObject.typeid);
    }

    private void SpawnEnemyBullet()
    {

    }
}
