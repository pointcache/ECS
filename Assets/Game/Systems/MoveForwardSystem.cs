﻿using System;
using System.Collections.Generic;
using ECS;
using UnityEngine;

public class MoveForwardSystem : ECSSystem
{

    [Inject(typeof(MoveForward), typeof(Position))] Match match;
    [Inject] ComponentTracker<MoveForward> mfs;
    [Inject] ComponentTracker<Position> positions;

    public MoveForwardSystem(ECSRoot root) : base(root)
    {
    }

    public override void Update(float delta)
    {
        for (int i = 0; i < match.Length; i++)
        {
            int id = match.Entities[i];
            MoveForward mf = mfs[id];
            Position pos = positions[id];

            float mag = mf.speed * delta;

            Vector2 forward = VectorUtils.GetForward(pos);

            pos.x += forward.x * mag;
            pos.y += forward.y * mag;
            
        }
    }
}
