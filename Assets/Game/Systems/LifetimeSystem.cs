﻿using System;
using System.Collections.Generic;
using ECS;

public class LifetimeSystem : ECSSystem
{
    [Inject(typeof(Lifetime))]
    private Match match;

    [Inject] ComponentTracker<Lifetime> lifetimes;

    public LifetimeSystem(ECSRoot root) : base(root)
    {
    }

    public override void Update(float delta)
    {
        for (int i = 0; i < match.Length; i++)
        {
            Entity e = match[i];

            Lifetime lt = lifetimes.ComponentsByEntity[e.ID];

            lt.lifetimeLeft -= delta;  

            if(lt.lifetimeLeft < 0)
            {
                PostUpdate.DestroyEntity(e.ID);
            }
        }
    }
}
