﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        float delta = Time.deltaTime;

        var pos = transform.position;

        pos.x += Input.GetAxisRaw("Horizontal") * delta * 5f;

        transform.position = pos;

	}
}
