﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ECS;
using System;

public class ECSDebugGUI : MonoBehaviour
{

    private ECSRoot root;
    private Vector2 scroll;
    private int fieldHeight = 20;
    private EntityInspector inspector = new EntityInspector();
    private int selectedEntity;

    internal void SetRoot(ECSRoot ECS) => this.root = ECS;

    private void OnGUI()
    {
        Rect rect = new Rect(20, 20, 200, 500);
        GUI.Box(rect, "");

        Rect viewRect = new Rect(rect.x, rect.y, rect.width, this.fieldHeight * this.root.EntityManager.Count);

        this.scroll = GUI.BeginScrollView(rect, this.scroll, viewRect);

        this.root.EntityManager.BufferUpdate();
        EntityBuffer buffer = this.root.EntityManager.GetBuffer();
        for (int i = 0; i < buffer.Length; i++)
        {
            Entity e = buffer.array[i];
            Rect br = new Rect(rect.x, rect.y + this.fieldHeight * i, rect.width - 30, this.fieldHeight);

            if(GUI.Button(br, e.Name + ":" + e.ID))
            {
                this.selectedEntity = e.ID;
                break;
            }

            if(GUI.Button(new Rect(br.x + br.width, br.y, 30, br.height), "x"))
            {
                this.root.EntityManager.DestroyEntity(e);
                break;
            }
        }

        GUI.EndScrollView();

        this.inspector.Inspect(this.selectedEntity, new Rect(250, 60, 600, 500), this.root);
    }
}
