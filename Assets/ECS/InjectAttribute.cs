﻿namespace ECS
{

    using System;

    /// <summary>
    /// Used by SystemsController to inject data into systems.
    /// </summary>
    [System.AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    sealed class InjectAttribute : Attribute
    {

        public Type[] Types { get; }

        public InjectAttribute(params Type[] types)
        {
            this.Types = types;
        }

    } 
}