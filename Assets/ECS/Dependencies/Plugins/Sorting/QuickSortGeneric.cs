﻿namespace ECS.Utility
{
    using UnityEngine;
    using System;
    using System.Linq;
    using System.Collections.Generic;

    public class QuickSorter<T> where T : IComparable
    {
        public void Quicksort(List<T> elements)
        {
            if (elements.Count < 2)
            {
                return;
            }

            quicksort(elements, 0, elements.Count - 1);
        }

        void quicksort(List<T> elements, int left, int right)
        {
            int i = left, j = right;
            IComparable pivot = elements[(left + right) / 2];

            while (i <= j)
            {
                while (elements[i].CompareTo(pivot) < 0)
                {
                    i++;
                }

                while (elements[j].CompareTo(pivot) > 0)
                {
                    j--;
                }

                if (i <= j)
                {
                    // Swap
                    T tmp = elements[i];
                    elements[i] = elements[j];
                    elements[j] = tmp;

                    i++;
                    j--;
                }
            }

            // Recursive calls
            if (left < j)
            {
                quicksort(elements, left, j);
            }

            if (i < right)
            {
                quicksort(elements, i, right);
            }
        }
    }


}