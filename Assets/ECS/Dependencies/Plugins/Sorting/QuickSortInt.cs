﻿namespace ECS.Utility
{
    public class QuickSortInt
    {
        public void Quicksort(int[] elements, int count)
        {
            if (count < 2)
            {
                return;
            }

            quicksort(elements, 0, count - 1);
        }

        void quicksort(int[] elements, int left, int right)
        {
            int i = left, j = right;
            int pivot = elements[(left + right) / 2];

            while (i <= j)
            {
                while (elements[i] > (pivot))
                {
                    i++;
                }

                while (elements[j] < (pivot))
                {
                    j--;
                }

                if (i <= j)
                {
                    // Swap
                    int tmp = elements[i];
                    elements[i] = elements[j];
                    elements[j] = tmp;

                    i++;
                    j--;
                }
            }

            // Recursive calls
            if (left < j)
            {
                quicksort(elements, left, j);
            }

            if (i < right)
            {
                quicksort(elements, i, right);
            }
        }
    } 
}