﻿
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

[System.Serializable]
public static class TypeUtility {

    public static bool HasAttribute(this MemberInfo info, Type attributeType) {
        return info.GetCustomAttributes(attributeType, false).Length > 0;
    }

    public static bool HasAttribute(this FieldInfo info, Type attributeType) {
        return info.GetCustomAttributes(attributeType, false).Length > 0;
    }

    public static bool HasAttribute(this PropertyInfo info, Type attributeType) {
        return info.GetCustomAttributes(attributeType, false).Length > 0;
    }

    /// <summary>
    /// Alternative version of <see cref="Type.IsSubclassOf"/> that supports raw generic types (generic types without
    /// any type parameters).
    /// </summary>
    /// <param name="baseType">The base type class for which the check is made.</param>
    /// <param name="toCheck">To type to determine for whether it derives from <paramref name="baseType"/>.</param>
    public static bool IsSubclassOfRawGeneric(this Type toCheck, Type baseType) {
        while (toCheck != typeof(object)) {
            Type cur = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;
            if (baseType == cur) {
                return true;
            }

            toCheck = toCheck.BaseType;
        }

        return false;
    }

}
