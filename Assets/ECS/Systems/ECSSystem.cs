﻿namespace ECS
{

    using System;
    using System.Collections.Generic;
    using ECS.Systems;
    using UnityEngine;

    /// <summary>
    /// Base class for Systems
    /// </summary>
    public abstract class ECSSystem
    {

        public readonly ECSRoot ecs;
        public readonly EntityManager entityManager;

        public virtual void Awake() { }
        public abstract void Update(float delta);
        public virtual void OnDrawGizmos(float delta) { }
        public virtual void OnGUI(float delta) { }

        protected readonly Commands PostUpdate;

        public ECSSystem(ECSRoot root)
        {
            this.ecs = root;
            this.entityManager = root.EntityManager;
            this.PostUpdate = new Commands(root);
        }

        public ISystemCommands GetCommands() => PostUpdate;

        protected class Commands : ISystemCommands
        {
            private ECSRoot root;
            private EntityManager manager;
            private int[] entityDestructionQueue;
            private int entityDestructionQueueCount;

            public Commands(ECSRoot root)
            {
                this.root = root;
                this.manager = root.EntityManager;

                this.entityDestructionQueue = new int[0];
            }

            void ISystemCommands.ExecuteCommands()
            {
                // Destroy Entities
                for (int i = 0; i < entityDestructionQueueCount; i++)
                {
                    manager.DestroyEntity(entityDestructionQueue[i]);
                }
                entityDestructionQueueCount = 0;
            }

            public void DestroyEntity(int entityID)
            {
                if (entityDestructionQueueCount + 1 > entityDestructionQueue.Length)
                {
                    int[] arr = new int[entityDestructionQueue.Length + ECSRoot.EntityBlockSize];
                    Array.Copy(entityDestructionQueue, arr, entityDestructionQueueCount);
                    entityDestructionQueue = arr;
                }

                entityDestructionQueue[entityDestructionQueueCount] = entityID;
                entityDestructionQueueCount++;
            }
        }


    }

}