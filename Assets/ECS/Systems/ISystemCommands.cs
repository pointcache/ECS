﻿namespace ECS.Systems
{

    using System;
    using System.Collections.Generic;

    public interface ISystemCommands
    {
        void ExecuteCommands();
    }

}