﻿namespace ECS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public interface IComponentAddedReceiver<T> where T : Comp
    {
        void OnCompAdded(T comp, Entity entity);
    }

}


