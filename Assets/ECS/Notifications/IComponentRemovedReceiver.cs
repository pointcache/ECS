﻿namespace ECS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public interface IComponentRemovedReceiver<T> where T : Comp
    {
        void OnCompRemoved(T comp, Entity entity);
    }

}


