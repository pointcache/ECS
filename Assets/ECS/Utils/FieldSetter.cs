﻿
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

/// <summary>
/// Generates a fast field Setter/Getter with IL Emit.
/// </summary>
public static class FieldSetter
{
    public static Action<object, object> BuildSetter(FieldInfo fld)
    {
        DynamicMethod dyn = new DynamicMethod("set_" + fld, typeof(void), new[] { typeof(object), typeof(object) }, fld.DeclaringType);
        ILGenerator gen = dyn.GetILGenerator();
        gen.Emit(OpCodes.Ldarg_0);
        gen.Emit(OpCodes.Castclass, fld.DeclaringType);
        gen.Emit(OpCodes.Ldarg_1);
        if (fld.FieldType.IsClass)
        {
            gen.Emit(OpCodes.Castclass, fld.FieldType);
        }
        else
        {
            gen.Emit(OpCodes.Unbox_Any, fld.FieldType);
        }
        gen.Emit(OpCodes.Stfld, fld);
        gen.Emit(OpCodes.Ret);
        return (Action<object, object>)dyn.CreateDelegate(typeof(Action<object, object>));
    }

    public static Func<object, object> BuildGetter(FieldInfo fld)
    {
        DynamicMethod dyn = new DynamicMethod("get_" + fld, typeof(void), new[] { typeof(object), typeof(object) }, fld.DeclaringType);
        ILGenerator gen = dyn.GetILGenerator();
        gen.Emit(OpCodes.Ldarg_0);
        if (fld.FieldType.IsClass)
        {
            gen.Emit(OpCodes.Castclass, fld.FieldType);
        }
        else
        {
            gen.Emit(OpCodes.Unbox_Any, fld.FieldType);
        }
        gen.Emit(OpCodes.Ldarg_1);
        gen.Emit(OpCodes.Castclass, fld.DeclaringType);
        gen.Emit(OpCodes.Ldfld, fld);
        gen.Emit(OpCodes.Ret);
        return (Func<object, object>)dyn.CreateDelegate(typeof(Func<object, object>));
    }
}
