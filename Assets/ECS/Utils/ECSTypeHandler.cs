﻿namespace ECS {

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using UnityEngine;

    /// <summary>
    /// Performs initial type gathering and caching for component types,
    /// injects runtime typeid into static "id" field of components.
    /// </summary>
    public static class ECSTypeHandler {

        public static List<Type> COMPONENT_TYPES = new List<Type>();
        public static Dictionary<Type, int> COMPONENT_TYPES_ID = new Dictionary<Type, int>();

        static ECSTypeHandler() {

            var a = Assembly.GetExecutingAssembly();
            var t = a.GetTypes();
            foreach (var item in t) {
                if (typeof(Comp).IsAssignableFrom(item) && !item.IsAbstract) {
                    COMPONENT_TYPES.Add(item);
                }
            }

            COMPONENT_TYPES = COMPONENT_TYPES.OrderBy(x => x.Name).ToList();

            for (int i = 0; i < COMPONENT_TYPES.Count; i++) {
                var type = COMPONENT_TYPES[i];
                var field = type.GetField("typeid", BindingFlags.Public | BindingFlags.Static);
                if (field == null) {
                    Debug.LogError(string.Format("ECS found a component class: {0} that does not have a readonly static typeid field. Please refer to docs on new component creation.", type.Name));
                    Application.Quit();
                }

                field.SetValue(null, i);
                COMPONENT_TYPES_ID.Add(type, i);
            }
        }
    }
}