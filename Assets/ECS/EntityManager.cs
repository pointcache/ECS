﻿namespace ECS
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ECS.Internal;
    using ECS.Utility;
    using UnityEngine;

    public class EntityManager
    {
        /// <summary>
        /// Total active entities count
        /// </summary>
        public int Count => entityPool.EntitiesCount;
        public static event Action<Entity> OnEntityDestroyed = delegate { };

        private EntityPool entityPool;

        /// <summary>
        /// Temporary buffer to perform GetAllComponents efficiently
        /// </summary>
        private List<Comp> getAllCompsBuffer = new List<Comp>();

        internal EntityBuffer GetBuffer()
        {
            return this.entityPool.existingBuffer;
        }

        /// <summary>
        /// Array of component pools stored by TypeID
        /// </summary>
        private ComponentTrackerBase[] trackers;

        /// <summary>
        /// This can be quite expensive when many entities in the world, use sparingly, for example at the end of a frame.
        /// Request this if you need access to buffer containing all existing entities.
        /// In other words - if you need to get an array of all entities compacted, call this before it,
        /// otherwise its not needed. 
        /// </summary>
        public void BufferUpdate()
        {
            entityPool.UpdateBuffer();
        }

        internal void Update()
        {
            entityPool.Update();
        }

        /// <summary>
        /// Manages and pools entities
        /// Entity IDs are reused, to fill "holes" when destroying entities with previously used ids.
        /// </summary>
        private class EntityPool
        {
            public Entity[] entities_by_id;
            public EntityBuffer existingBuffer;
            public int EntitiesCount = 0;

            private int blocksize;
            private int totalsize = 0;

            /// <summary>
            /// How many new ids are generated when needed.
            /// </summary>
            private int idBatch = 10;

            /// <summary>
            /// Sorts free ids, optional
            /// </summary>
            private QuickSortInt sorter = new QuickSortInt();

            private int[] unusedIDs;
            private int unusedIDsCount;

            /// <summary>
            /// Reference to manager
            /// </summary>
            private EntityManager manager;

            /// <summary>
            /// Free unused entities
            /// </summary>
            private Entity[] free;
            private int freeCount;

            /// <summary>
            /// How many entities will be created when needed
            /// </summary>
            private int entityBatchSize = 100;

            public EntityPool(EntityManager manager)
            {
                this.manager = manager;
                blocksize = ECSRoot.EntityBlockSize;
                entities_by_id = new Entity[0];

                existingBuffer = new EntityBuffer(0);
                unusedIDs = new int[ECSRoot.EntityBlockSize];
                free = new Entity[ECSRoot.EntityBlockSize];
                Expand();
            }

            /// <summary>
            /// Grabs free entity from the pool and initializes it
            /// </summary>
            /// <param name="name">Name of the entity</param>
            /// <param name="databaseID">Database ID of the entity</param>
            /// <returns></returns>
            public Entity GetNew(string name = null, string databaseID = null)
            {
                if (EntitiesCount + 1 > totalsize)
                {
                    Expand();
                }

                int id = GetId();

                Entity e = GetEntity();

                e.ID = id;
                e.DBID = databaseID;
                e.Manager = manager;
                e.ComponentsCount = 0;
                e.Name = name ?? "Entity";

                entities_by_id[id] = e;
                EntitiesCount++;
                return e;
            }

            public void Destroy(Entity e)
            {
                int id = e.ID;

                // return id to the free ids pool
                if (unusedIDsCount + 1 > unusedIDs.Length)
                {
                    int[] old = unusedIDs;
                    unusedIDs = new int[unusedIDsCount + ECSRoot.PoolBlockSize];
                    Array.Copy(old, unusedIDs, unusedIDsCount);
                }
                unusedIDs[unusedIDsCount] = id;
                unusedIDsCount++;


                entities_by_id[id] = null;

                // Return entity to the free pool
                if (free.Length < freeCount + 1)
                {
                    var arr = new Entity[freeCount + blocksize];
                    Array.Copy(free, arr, freeCount);
                    free = arr;
                }
                free[freeCount] = e;
                freeCount++;
                EntitiesCount--;
            }

            public void Update()
            {
                if (ECSRoot.SortIDS)
                {
                    sorter.Quicksort(unusedIDs, unusedIDsCount);
                }
            }

            /// <summary>
            /// Prepares existing entities buffer for use
            /// </summary>
            public void UpdateBuffer()
            {
                existingBuffer.Length = 0;
                int idIndex = 0;

                // Iterate over all entities and fill the buffer with them, skips nulls
                for (int i = 0; i < EntitiesCount; i++)
                {
                    while (entities_by_id[idIndex] == null)
                    {
                        idIndex++;
                    }
                    existingBuffer.array[i] = entities_by_id[idIndex];
                    existingBuffer.Length++;
                    idIndex++;
                }
            }

            /// <summary>
            /// Gets a free entity
            /// </summary>
            /// <returns></returns>
            private Entity GetEntity()
            {
                // Allocate new batch if no free entities
                if (freeCount == 0)
                {
                    if (free.Length < entityBatchSize)
                    {
                        var arr = new Entity[free.Length + entityBatchSize + this.blocksize];
                        Array.Copy(free, arr, freeCount);
                        free = arr;
                    }
                    for (int i = 0; i < entityBatchSize; i++)
                    {
                        free[i] = new Entity();
                        freeCount++;
                    }
                }

                Entity e = free[freeCount - 1];
                freeCount--;
                return e;
            }

            /// <summary>
            /// Expands internal arrays
            /// </summary>
            private void Expand()
            {
                int newsize = EntitiesCount + blocksize;

                var arr = new Entity[newsize];
                Array.Copy(entities_by_id, arr, EntitiesCount);
                entities_by_id = arr;
                existingBuffer.array = new Entity[newsize];

                totalsize += blocksize;
            }

            /// <summary>
            /// Returns a free id
            /// </summary>
            /// <returns></returns>
            private int GetId()
            {
                if (unusedIDsCount == 0)
                {
                    for (int i = EntitiesCount; i < EntitiesCount + idBatch; i++)
                    {
                        unusedIDs[unusedIDsCount] = i;
                        unusedIDsCount++;
                    }
                }

                int id = unusedIDs[unusedIDsCount - 1];
                unusedIDsCount--;
                return id;
            }

            public void ResetIDCount()
            {
                unusedIDsCount = 0;
            }
        }

        internal EntityManager()
        {
            entityPool = new EntityPool(this);

            trackers = new ComponentTrackerBase[ECSTypeHandler.COMPONENT_TYPES.Count];

            // Create a pool for each type of detected component
            for (int i = 0; i < ECSTypeHandler.COMPONENT_TYPES.Count; i++)
            {
                Type t = ECSTypeHandler.COMPONENT_TYPES[i];

                if (typeof(SharedComp).IsAssignableFrom(t))
                {
                    var d1 = typeof(SharedComponentTracker<>);
                    Type[] typeArgs = new Type[] { ECSTypeHandler.COMPONENT_TYPES[i] };
                    var makeme = d1.MakeGenericType(typeArgs);

                    ComponentTrackerBase tracker = (ComponentTrackerBase)Activator.CreateInstance(makeme, i, t, this);
                    trackers[i] = tracker;
                }
                else
                if (typeof(SingletonComp).IsAssignableFrom(t))
                {
                    var d1 = typeof(SingletonTracker<>);
                    Type[] typeArgs = new Type[] { ECSTypeHandler.COMPONENT_TYPES[i] };
                    var makeme = d1.MakeGenericType(typeArgs);

                    ComponentTrackerBase pool = (ComponentTrackerBase)Activator.CreateInstance(makeme, i, t, this);
                    trackers[i] = pool;
                }
                else
                {
                    var d1 = typeof(ComponentTracker<>);
                    Type[] typeArgs = new Type[] { ECSTypeHandler.COMPONENT_TYPES[i] };
                    var makeme = d1.MakeGenericType(typeArgs);

                    ComponentTrackerBase pool = (ComponentTrackerBase)Activator.CreateInstance(makeme, i, t, this);
                    trackers[i] = pool;
                }

            }
        }

        internal Entity CreateEntity(string name = null, string databaseID = null) => entityPool.GetNew(name, databaseID);

        internal Entity GetEntity(int entityID) => entityPool.entities_by_id[entityID];

        internal void DestroyEntity(Entity e)
        {
            int[] comps = e.Components;
            for (int i = 0; i < e.ComponentsCount; i++)
            {
                trackers[comps[i]].Destroy(e, true);
            }
            e.ComponentsCount = 0;
            entityPool.Destroy(e);
        }


        internal void DestroyEntity(int id)
        {
            Entity e = entityPool.entities_by_id[id];
            DestroyEntity(e);
        }

        internal void DestroyAllEntities()
        {
            entityPool.UpdateBuffer();
            var buffer = entityPool.existingBuffer;
            var array = buffer.array;
            for (int i = 0; i < buffer.Length; i++)
            {
                DestroyEntity(array[i]);
            }
            entityPool.ResetIDCount();
        }

        internal bool Exists(int runtimeID) => entityPool.entities_by_id[runtimeID] != null;

        internal Comp AddComponent(int typeid, Entity entity)
        {
            if (entity == null)
            {
                Debug.LogError("Tried to add component to a null entity.");
                return null;
            }
            Comp c = trackers[typeid].Add(entity);
            return c;
        }

        internal void AddComponent<T>(T comp, Entity entity) where T : Comp
        {
            if (entity == null)
            {
                Debug.LogError("Tried to add component to a null entity.");
                return;
            }
            trackers[comp.Typeid].AddExisting(comp, entity);
        }

        internal Comp GetComponent(int typeid, Entity entity) => trackers[typeid].Get(entity.ID);
        internal Comp GetComponent(int typeid, int entityID) => trackers[typeid].Get(entityID);

        internal Comp GetSingletonComponent(int typeid) => trackers[typeid].GetAny();

        internal void DestroyComponent(int typeid, Entity entity) => trackers[typeid].Destroy(entity);

        /// <summary>
        /// Fills the buffer with all existing components on an entity
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="buffer"></param>
        internal void GetAllComponents(Entity entity, List<Comp> buffer)
        {
            buffer.Clear();
            int id = entity.ID;
            int[] comps = entity.Components;

            for (int i = 0; i < entity.ComponentsCount; i++)
            {
                buffer.Add(trackers[comps[i]].Get(id));
            }
        }

        /// <summary>
        /// Returns a copy of all pools
        /// </summary>
        /// <returns></returns>
        internal ComponentTrackerBase[] GetPools() => trackers.ToArray();

        internal ComponentTrackerBase GetPool(int typeid) => trackers[typeid];
    }

}