﻿namespace ECS
{

    using System;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// Statically stores matching entities
    /// </summary>
    public class Matcher
    {

        private static int matchersCount = 0;

        public readonly int MatcherID;

        /// <summary>
        /// Which types this entity has to match
        /// </summary>
        public int[] ComponentTypeIds;

        /// <summary>
        /// Resulting entities IDs
        /// </summary>
        public int[] MatchingEntityIDs;
        public int Length;

        /// <summary>
        /// Hashset used here for fast insert/removal (mostly removal)
        /// sadly seems unavoidable
        /// </summary>
        public HashSet<int> matches = new HashSet<int>();

        protected EntityManager manager;
        protected int blocksize = ECSRoot.MatcherBlockSize;
        protected int size = 0;

        /// <summary>
        /// Small optimization 
        /// </summary>
        public bool Changed;

        public Matcher(EntityManager manager, int[] typeids)
        {
            this.manager = manager;
            this.ComponentTypeIds = typeids;

            this.MatcherID = matchersCount;
            matchersCount++;
            Expand();
        }

        private void Expand()
        {
            int count = this.matches.Count;
            if (this.size < count)
            {
                this.size = count;
            }
            this.size += this.blocksize;
            this.MatchingEntityIDs = new int[this.size];
        }

        /// <summary>
        /// Copies the matches from the hashset to array
        /// </summary>
        public void Prepare()
        {
            if (this.Changed)
            {
                this.Length = this.matches.Count;

                if (this.Length > this.size)
                {
                    Expand();
                }
                    
                this.matches.CopyTo(this.MatchingEntityIDs, 0, this.Length);
            }

            this.Changed = false;
        }
    }

}