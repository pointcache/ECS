﻿namespace ECS
{

    using System;
    using System.Collections.Generic;
    using UnityEngine;

    [System.Serializable]
    public class Entity
    {
        /// <summary>
        /// Runtime ID
        /// </summary>
        public int ID;

        [EditorSave]
        public string Name;

        /// <summary>
        /// Database ID
        /// </summary>
        [EditorSave]
        public string DBID;



        /// <summary>
        /// Cached reference to entity manager
        /// </summary>
        [fsIgnore]
        public EntityManager Manager;

        [fsIgnore]
        public bool Destroyed => !this.Manager.Exists(this.ID);

        /// <summary>
        /// Internal components array, stores IDS of components attached to this entity,
        /// used to optimize destruction of entity
        /// </summary>
        [fsIgnore]
        public int[] Components;
        public int ComponentsCount;


        public Entity()
        {
            this.Components = new int[ECSRoot.EntityInternalComponentArrayBlockSize];
        }

        public Comp AddComponent(int typeid) => this.Manager.AddComponent(typeid, this);

        public void AddComponent<T>(T comp) where T : Comp
        {
            this.Manager.AddComponent(comp, this);
        }

        public void GetAllComps(List<Comp> buffer) => this.Manager.GetAllComponents(this, buffer);

        internal void DestroyComponent(int r_typeid) => this.Manager.DestroyComponent(r_typeid, this);
    }

}