﻿namespace ECS
{

    using System;
    using System.Collections.Generic;

    public static class ComponentPool<T> where T : Comp
    {

        private static int typeid;

        /// <summary>
        /// How much the internal arrays will grow.
        /// </summary>
        private static int blockSize = ECSRoot.PoolBlockSize;

        /// <summary>
        /// How many components will be preallocated if no free components left
        /// </summary>
        private static int compBatchSize = ECSRoot.PoolPreAllocationBatch;

        private static ECSObjectActivator.Activator.ObjectActivator activator;

        /// <summary>
        /// Unused/free components
        /// </summary>
        private static T[] free;
        private static int freeCount;


        static ComponentPool()
        {
            typeid = ECSTypeHandler.COMPONENT_TYPES_ID[typeof(T)];
            free = new T[0];
            activator = ECSObjectActivator.CompActivators[typeid].activator;
        }

        public static T Next
        {
            get
            {
                // Find a free component
                if (freeCount == 0)
                {
                    AllocateBatch();
                }
                freeCount--;
                return free[freeCount];
            }
        }

        public static T Get()
        {
            // Find a free component
            if (freeCount == 0)
            {
                AllocateBatch();
            }
            freeCount--;
            return free[freeCount];
        }

        public static void Release(T c)
        {
            // Move component to free 
            if (free.Length < freeCount + 1)
            {
                T[] arr = new T[freeCount + blockSize];
                Array.Copy(free, arr, freeCount);
                free = arr;
            }
            free[freeCount] = c;
            freeCount++;
        }

        private static void AllocateBatch()
        {
            // Expand
            if (free.Length < freeCount + compBatchSize)
            {
                T[] arr = new T[freeCount + compBatchSize + blockSize];
                Array.Copy(free, arr, freeCount);
                free = arr;
            }

            // Allocate
            for (int i = 0; i < compBatchSize; i++)
            {
                T c = (T)activator();
                free[freeCount] = c;
                freeCount++;
            }
        }
    }

}