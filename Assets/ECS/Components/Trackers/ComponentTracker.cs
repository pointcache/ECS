﻿namespace ECS
{
    using System;
    using ECS.Internal;
    using UnityEngine;
    /// <summary>
    /// Tracks active components
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ComponentTracker<T> : ComponentTrackerBase where T : Comp
    {

        /// <summary>
        /// Used components, stored by entity ID
        /// </summary>
        public T[] ComponentsByEntity;
        public T[] Components;

        public IComponentAddedReceiver<T>[] onAddedReceivers;
        public IComponentRemovedReceiver<T>[] onRemovedReceivers;

        public T this[int id]
        {
            get
            {
                return this.ComponentsByEntity[id];
            }
        }


        public ComponentTracker(int typeid, Type type, EntityManager manager) : base(typeid, type, manager)
        {
            this.onAddedReceivers = new IComponentAddedReceiver<T>[0];
            this.onRemovedReceivers = new IComponentRemovedReceiver<T>[0];
            this.ComponentsByEntity = new T[this.blockSize];
            this.Components = new T[this.blockSize];
        }

        public override Comp Add(Entity entity) => AddGeneric(entity);

        public override Comp Get(int id)
        {
            if (id > this.ComponentsByEntity.Length - 1)
            {
                return null;
            }
            else
            {
                return this.ComponentsByEntity[id];
            }
        }

        public T GetGeneric(Entity e)
        {
            if (e == null)
                return null;
            return GetGeneric(e.ID);
        }

        public T GetGeneric(int id)
        {
            if (id > this.ComponentsByEntity.Length - 1)
            {
                return null;
            }
            return ComponentsByEntity[id];
        }

        public T AddGeneric(Entity entity)
        {
            int entityID = entity.ID;

            // Expansion by entity id
            if (this.currentSize < entityID + 1)
            {
                var arr = new T[this.ComponentsByEntity.Length + this.blockSize];
                Array.Copy(this.ComponentsByEntity, arr, this.ComponentsByEntity.Length);
                this.ComponentsByEntity = arr;
                this.currentSize = this.ComponentsByEntity.Length - 1;
            }

            if (this.ComponentsByEntity[entityID] != null)
            {
                Debug.Log("Entity: " + entityID + " already has a component of type: " + this.type.Name);
                return null;
            }

            // Expansion all comps
            if (this.Components.Length < count + 1)
            {
                var arr = new T[this.Components.Length + this.blockSize];
                Array.Copy(this.Components, arr, this.Components.Length);
                this.Components = arr;
            }

            T c = ComponentPool<T>.Get();

            this.ComponentsByEntity[entityID] = c;
            this.Components[count] = c;
            this.count++;

            // Add component to internal entity component array
            int[] comps = entity.Components;
            if (entity.ComponentsCount + 1 > comps.Length)
            {
                int[] newarr = new int[entity.ComponentsCount + ECSRoot.EntityInternalComponentArrayBlockSize];
                Array.Copy(comps, newarr, entity.ComponentsCount);
                entity.Components = newarr;
            }

            entity.Components[entity.ComponentsCount] = typeid;
            entity.ComponentsCount++;

            c.EntityID = entityID;

            // Process matchers add
            int mcount = this.matchers.Length;
            for (int i = 0; i < mcount; i++)
            {
                Matcher m = this.matchers[i];
                bool match = true;
                foreach (int id in m.ComponentTypeIds)
                {
                    if (this.manager.GetComponent(id, entityID) == null)
                    {
                        match = false;
                    }
                }

                if (match)
                {
                    m.matches.Add(entityID);
                    m.Changed = true;
                }
            }

            // Process On Added Notification
            for (int i = 0; i < onAddedReceivers.Length; i++)
            {
                this.onAddedReceivers[i].OnCompAdded(c, entity);
            }

            return c;
        }


        public override void AddExisting(Comp comp, Entity entity)
        {
            int entityID = entity.ID;

            // Expansion
            if (this.currentSize < entityID + 1)
            {
                var arr = new T[this.ComponentsByEntity.Length + this.blockSize];
                Array.Copy(this.ComponentsByEntity, arr, this.ComponentsByEntity.Length);
                this.ComponentsByEntity = arr;
                this.currentSize = this.ComponentsByEntity.Length - 1;
            }

            if (this.ComponentsByEntity[entityID] != null)
            {
                Debug.Log("Entity: " + entityID + " already has a component of type: " + this.type.Name);
                return;
            }

            // Expansion all comps
            if (this.Components.Length < count + 1)
            {
                var arr = new T[this.Components.Length + this.blockSize];
                Array.Copy(this.Components, arr, this.Components.Length);
                this.Components = arr;
            }

            T c = (T)comp;

            this.ComponentsByEntity[entityID] = c;
            this.Components[count] = c;
            this.count++;

            // Add component to internal entity component array
            int[] comps = entity.Components;
            if (entity.ComponentsCount + 1 > comps.Length)
            {
                int[] newarr = new int[entity.ComponentsCount + ECSRoot.EntityInternalComponentArrayBlockSize];
                Array.Copy(comps, newarr, entity.ComponentsCount);
                entity.Components = newarr;
            }

            entity.Components[entity.ComponentsCount] = typeid;
            entity.ComponentsCount++;

            c.EntityID = entityID;

            // Process matchers add
            int mcount = this.matchers.Length;
            for (int i = 0; i < mcount; i++)
            {
                Matcher m = this.matchers[i];
                bool match = true;
                foreach (int id in m.ComponentTypeIds)
                {
                    if (this.manager.GetComponent(id, entityID) == null)
                    {
                        match = false;
                    }
                }

                if (match)
                {
                    m.matches.Add(entityID);
                    m.Changed = true;
                }
            }

            // Process On Added Notification
            for (int i = 0; i < onAddedReceivers.Length; i++)
            {
                this.onAddedReceivers[i].OnCompAdded(c, entity);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="skipEntityComponentArrayRemoval"> If true wont do the shifting and removal on internal entity component array. Used in DestroyEntity for example </param>
        public override void Destroy(Entity entity, bool skipEntityComponentArrayRemoval = false)
        {
            int entityID = entity.ID;

            // TODO : add exception or any handling of this
            if (this.currentSize < entityID)
            {
                return;
            }

            T c = this.ComponentsByEntity[entityID];
            if (c != null)
            {

                this.ComponentsByEntity[entityID] = null;

                ComponentPool<T>.Release(c);

                // Here we find component type id in the internal entity comp array and
                // shift the rest of array
                // Or we skip it completely since DestroyEntity will take care of it
                if (!skipEntityComponentArrayRemoval)
                {
                    int[] comps = entity.Components;
                    bool found = false;
                    for (int i = 0; i < entity.ComponentsCount; i++)
                    {
                        if (comps[i] == typeid)
                        {
                            found = true;
                        }
                        if (found)
                        {
                            if (i + 1 < entity.ComponentsCount)
                            {
                                comps[i] = comps[i + 1];
                            }
                        }
                    }
                    entity.ComponentsCount--;
                }
                this.count--;
                this.Components[this.count] = null;
            }

            // Process matchers remove
            int count = this.matchers.Length;
            for (int i = 0; i < count; i++)
            {
                Matcher m = this.matchers[i];
                m.matches.Remove(entityID);
                m.Changed = true;
            }

            // Process On Removed Notification
            for (int i = 0; i < onRemovedReceivers.Length; i++)
            {
                this.onRemovedReceivers[i].OnCompRemoved(c, entity);
            }

            c.Reset();
            count--;
        }

        public override void RegisterSystemCompAdded(ECSSystem system)
        {
            var arr = new IComponentAddedReceiver<T>[onAddedReceivers.Length + 1];
            Array.Copy(this.onAddedReceivers, arr, this.onAddedReceivers.Length);
            arr[this.onAddedReceivers.Length] = (IComponentAddedReceiver<T>)system;
            this.onAddedReceivers = arr;
        }

        public override void RegisterSystemCompRemoved(ECSSystem system)
        {
            var arr = new IComponentRemovedReceiver<T>[onRemovedReceivers.Length + 1];
            Array.Copy(this.onRemovedReceivers, arr, this.onRemovedReceivers.Length);
            arr[this.onRemovedReceivers.Length] = (IComponentRemovedReceiver<T>)system;
            this.onRemovedReceivers = arr;
        }
    }
}