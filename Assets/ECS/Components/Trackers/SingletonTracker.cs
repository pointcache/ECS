﻿namespace ECS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ECS.Internal;
    using UnityEngine;

    /// <summary>
    /// Tracks active singleton component
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SingletonTracker<T> : ComponentTrackerBase where T : Comp
    {

        /// <summary>
        /// Used components, stored by entity ID
        /// </summary>
        private T component;
        private int entityID;

        public T Component => component;

        public IComponentAddedReceiver<T>[] onAddedReceivers;
        public IComponentRemovedReceiver<T>[] onRemovedReceivers;


        private static ECSObjectActivator.Activator.ObjectActivator activator;

        public SingletonTracker(int typeid, Type type, EntityManager manager) : base(typeid, type, manager)
        {
            this.onAddedReceivers = new IComponentAddedReceiver<T>[0];
            this.onRemovedReceivers = new IComponentRemovedReceiver<T>[0];

            activator = ECSObjectActivator.CompActivators[typeid].activator;
        }

        public override Comp Add(Entity entity) => AddGeneric(entity);

        public override Comp Get(int id)
        {
            if (id != this.entityID)
            {
                return null;
            }
            else
            {
                return this.component;
            }
        }

        public override Comp GetAny() => this.component;

        public T AddGeneric(Entity entity)
        {

            if (component != null)
            {
                Debug.Log("Cant add another singleton component. " + this.type.Name);
                return null;
            }

            this.entityID = entity.ID;
            component = (T)activator();

            // Add component to internal entity component array
            int[] comps = entity.Components;
            if (entity.ComponentsCount + 1 > comps.Length)
            {
                int[] newarr = new int[entity.ComponentsCount + ECSRoot.EntityInternalComponentArrayBlockSize];
                Array.Copy(comps, newarr, entity.ComponentsCount);
                entity.Components = newarr;
            }

            entity.Components[entity.ComponentsCount] = typeid;
            entity.ComponentsCount++;

            component.EntityID = entityID;

            // Process matchers add
            int mcount = this.matchers.Length;
            for (int i = 0; i < mcount; i++)
            {
                Matcher m = this.matchers[i];
                bool match = true;
                foreach (int id in m.ComponentTypeIds)
                {
                    if (this.manager.GetComponent(id, entityID) == null)
                    {
                        match = false;
                    }
                }

                if (match)
                {
                    m.matches.Add(entityID);
                    m.Changed = true;
                }
            }

            // Process On Added Notification
            for (int i = 0; i < onAddedReceivers.Length; i++)
            {
                this.onAddedReceivers[i].OnCompAdded(component, entity);
            }

            count++;
            return component;
        }

        public override void AddExisting(Comp comp, Entity entity)
        {
            if (component != null)
            {
                Debug.Log("Cant add another singleton component. " + this.type.Name);
                return;
            }
            this.entityID = entity.ID;
            component = (T)comp;

            // Add component to internal entity component array
            int[] comps = entity.Components;
            if (entity.ComponentsCount + 1 > comps.Length)
            {
                int[] newarr = new int[entity.ComponentsCount + ECSRoot.EntityInternalComponentArrayBlockSize];
                Array.Copy(comps, newarr, entity.ComponentsCount);
                entity.Components = newarr;
            }

            entity.Components[entity.ComponentsCount] = typeid;
            entity.ComponentsCount++;

            component.EntityID = entityID;

            // Process matchers add
            int mcount = this.matchers.Length;
            for (int i = 0; i < mcount; i++)
            {
                Matcher m = this.matchers[i];
                bool match = true;
                foreach (int id in m.ComponentTypeIds)
                {
                    if (this.manager.GetComponent(id, entityID) == null)
                    {
                        match = false;
                    }
                }

                if (match)
                {
                    m.matches.Add(entityID);
                    m.Changed = true;
                }
            }

            // Process On Added Notification
            for (int i = 0; i < onAddedReceivers.Length; i++)
            {
                this.onAddedReceivers[i].OnCompAdded(component, entity);
            }
            count++;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="skipEntityComponentArrayRemoval"> If true wont do the shifting and removal on internal entity component array. Used in DestroyEntity for example </param>
        public override void Destroy(Entity entity, bool skipEntityComponentArrayRemoval = false)
        {
            if (this.component == null || entity.ID != this.entityID)
            {
                return;
            }

            // Here we find component type id in the internal entity comp array and
            // shift the rest of array
            // Or we skip it completely since DestroyEntity will take care of it
            if (!skipEntityComponentArrayRemoval)
            {
                int[] comps = entity.Components;
                bool found = false;
                for (int i = 0; i < entity.ComponentsCount; i++)
                {
                    if (comps[i] == typeid)
                    {
                        found = true;
                    }
                    if (found)
                    {
                        if (i + 1 < entity.ComponentsCount)
                        {
                            comps[i] = comps[i + 1];
                        }
                    }
                }
                entity.ComponentsCount--;
            }

            // Process matchers remove
            int count = this.matchers.Length;
            for (int i = 0; i < count; i++)
            {
                Matcher m = this.matchers[i];
                m.matches.Remove(entityID);
                m.Changed = true;
            }

            // Process On Removed Notification
            for (int i = 0; i < onRemovedReceivers.Length; i++)
            {
                this.onRemovedReceivers[i].OnCompRemoved(component, entity);
            }

            component.Reset();
            this.component = null;
            count--;
        }

        public override void RegisterSystemCompAdded(ECSSystem system)
        {
            var arr = new IComponentAddedReceiver<T>[onAddedReceivers.Length + 1];
            Array.Copy(this.onAddedReceivers, arr, this.onAddedReceivers.Length);
            arr[this.onAddedReceivers.Length] = (IComponentAddedReceiver<T>)system;
            this.onAddedReceivers = arr;
        }

        public override void RegisterSystemCompRemoved(ECSSystem system)
        {
            var arr = new IComponentRemovedReceiver<T>[onRemovedReceivers.Length + 1];
            Array.Copy(this.onRemovedReceivers, arr, this.onRemovedReceivers.Length);
            arr[this.onRemovedReceivers.Length] = (IComponentRemovedReceiver<T>)system;
            this.onRemovedReceivers = arr;
        }
    }
}