﻿namespace ECS
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// Base type for ECS component
    /// </summary>
    [System.Serializable]
    public abstract class Comp 
    {

        private int _typeid = -1;

        /// <summary>
        /// Runtime type id, lazily cached
        /// </summary>
        public abstract int Typeid { get; }

        /// <summary>
        /// Called every time the component is grabbed from the free pool.
        /// </summary>
        public abstract void Reset();

        public int EntityID = -1;



    }

}