﻿namespace ECS
{

    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public abstract class Archetype
    {
        public abstract Entity Construct();
    }

}